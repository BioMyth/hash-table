/*
	Samuel Casteel
	COMP 322
	This is the driver file to test the HashTable class created in the Hash Table file.

*/


#include <iostream>
#include <string>
#include "Hash Table.h"

// This is the functor class
// I use template specification so that there are individual types available through this single interface
template<typename T>
class hasher{
public:
	int operator()(T input);
};


int main(){
	//	Test the string case for the HashTable
	HashTable<int,std::string,hasher<std::string>> testString((hasher<std::string>()));
	testString.insert(120,"Here");
	for(int i = 1; i < 154; i++){
		testString.insert(i,std::to_string(i));
	}
	try{
		std::cout<<testString["Here"]<<std::endl;		// exists
		std::cout<<testString.find("a")<<std::endl;		// doesn't exist
	}
	catch(const std::invalid_argument& e){
		std::cout<<e.what()<<std::endl;
	}
	//	Test the int case for the HashTable
	HashTable<int,int,hasher<int>> testInt(hasher<int>(), 1.5);
	testInt.insert(120,500000);
	for(int i = 1; i < 154; i++){
		testInt.insert(154-i,i);
	}
	try{
		std::cout<<testInt.find(39)<<std::endl;	// exists
		std::cout<<testInt[500]<<std::endl;		// doesn't exist
	}
	catch(const std::invalid_argument& error){
		std::cout<<error.what()<<std::endl;
	}
	// Test the float case for the HashTable
	HashTable<int,float,hasher<float>> testFloat(hasher<float>(), 1, 50);
	testFloat.insert(120,500000);
	for(int i = 1; i < 154; i++){
		testFloat.insert(i,i*2.54f);
	}
	try{
		std::cout<<testFloat.find(13*2.54f)<<std::endl;	// exists
		std::cout<<testFloat[.00123456789f]<<std::endl;	// doesn't exist
	}
	catch(const std::invalid_argument& error){
		std::cout<<error.what()<<std::endl;
	}
	// this is just here to be able to see the output easily
	std::cin.ignore();
	return 0;
}


template<>
int hasher<std::string>::operator()(std::string input){
	// here I use the default hashing for strings provided by the hash class
	return (std::hash<std::string>()(input));
}

template<>
int hasher<int>::operator()(int input){
	// here I use two very large prime numbers one for multiplication and the other for division to attempt to create randomness
	return ((input * 179424691) / 179424347) % INT_MAX;
}

template<>
int hasher<float>::operator()(float input){
	// Here I am multiplying the double by a large number to attempt to create radomness
	return (int) (INT_MAX - (input * 24691));
}
