/*
	Samuel Casteel
	COMP 322
	This is the header file containing the HashTable class which is used to store key/value pairs.

*/

#pragma once

#ifndef __HASH_TABLE_H__
#define __HASH_TABLE_H__

#include <vector>

template<typename DataType, typename KeyType, typename Functor>
class HashTable{
public:
	// Default constructor using default variables so that not all of them have to be provided unless needed
	HashTable(Functor functor, float loadFactor = .8, int size = 20);
	// Both of these are ways of looking up if a key is in the table and if not it throws a bad argument error
	DataType find(KeyType key);
	DataType operator[] (KeyType key);
	// This inserts a key into the table(even if it already exists)
	void insert(DataType data, KeyType key);
	// Allows you to update the allowed load factor and if you are over it then it grow the table
	void setLoadFactor(float loadFactor);
private:
	// the data structure for each key/value pair
	struct Indice{
		Indice(DataType data,KeyType key):data(data), key(key){}
		DataType data;
		KeyType key;
	};

	std::vector<std::vector<Indice>> table;
	int valuesStored;
	float loadFactor;
	Functor hasher;
	void grow();
};

template<typename DataType, typename KeyType, typename Functor>
HashTable<DataType,KeyType,Functor>::HashTable(Functor functor, float loadFactor, int size):valuesStored(0), loadFactor(loadFactor), table(size), hasher(functor){
	this->table.resize(size);
}

template<typename DataType,typename KeyType, typename Functor>
DataType HashTable<DataType, KeyType, Functor>::operator[](KeyType key){
	int index = this->hasher(key) % this->table.size();

	for (auto chainIter = this->table[index].begin(); chainIter != this->table[index].end(); ++chainIter){
		if(chainIter->key == key)
			return chainIter->data;
	}

	throw std::invalid_argument("Invalid key. Key does not exist.");
}

template<typename DataType, typename KeyType, typename Functor>
void HashTable<DataType, KeyType, Functor>::setLoadFactor(float loadFactor){
	this->loadFactor = loadFactor;
	if(this->valuesStored/((float)this->table.capacity()) >= this->loadFactor)
			this->grow();
}

template<typename DataType, typename KeyType, typename Functor>
DataType HashTable<DataType,KeyType,Functor>::find(KeyType key){
	// uses this [] operator to lookup the value(why rewrite the same code ;))
	return (*this)[key];
}

template<typename DataType, typename KeyType, typename Functor>
void HashTable<DataType,KeyType,Functor>::insert(DataType data, KeyType key){

	int index = this->hasher(key) % this->table.capacity();

	this->table[index].push_back(Indice(data,key));
	this->valuesStored++;
	
	if (this->valuesStored/((float)this->table.capacity()) >= this->loadFactor)
		this->grow();
}

// doubles the capacity of the main table and reinserts all of its values
template<typename DataType, typename KeyType, typename Functor>
void HashTable<DataType,KeyType,Functor>::grow(){
	
	std::vector<std::vector<Indice>> oldTable(table);
	// clean out original vector
	this->table.clear();
	// double the capacity of the table
	this->table.resize(table.capacity() * 2);
	// reset the number of values stored so that insert won't cause issues
	this->valuesStored = 0;
	
	for(auto outerVectorIter = oldTable.begin(); outerVectorIter != oldTable.end(); ++outerVectorIter){
		for(auto innerVectorIter = outerVectorIter->begin(); innerVectorIter != outerVectorIter->end(); ++innerVectorIter){
			this->insert(innerVectorIter->data, innerVectorIter->key);
		}
	}
}

#endif